import React from 'react';
import './App.css';
import PointTarget from 'react-point'

const AutoScalingText = props => {
    const [scale, setScale] = React.useState(1);
    const ref = React.useRef(null);

    React.useLayoutEffect(() => {
        const node = ref.current;
        const parentNode = ref.current.parentNode;

        const availableWidth = parentNode.offsetWidth;
        const actualWidth = node.offsetWidth;
        const actualScale = availableWidth / actualWidth;

        if (scale === actualScale)
            return;

        if (actualScale < 1) {
            setScale(actualScale)
        } else if (scale < 1) {
            setScale(1)
        }
    });

    return (
        <div
            className="auto-scaling-text"
            style={{transform: `scale(${scale},${scale})`}}
            ref={ref}
        >{props.children}</div>
    )
};

const CalculatorDisplay = ({value, ...props}) => {
    const language = navigator.language || 'en-US'
    let formattedValue = parseFloat(value).toLocaleString(language, {
        useGrouping: true,
        maximumFractionDigits: 6
    })

    // Add back missing .0 in e.g. 12.0
    const match = value.match(/\.\d*?(0*)$/)

    if (match)
        formattedValue += (/[1-9]/).test(match[0]) ? match[1] : match[0]

    return (
        <div {...props} className="calculator-display">
            <AutoScalingText>{formattedValue}</AutoScalingText>
        </div>
    )
};

const CalculatorKey = ({onPress, className, ...props}) => {
    return (
        <PointTarget onPoint={onPress}>
            <button className={`calculator-key ${className}`} {...props}/>
        </PointTarget>
    )
};

const CalculatorOperations = {
    '/': (prevValue, nextValue) => prevValue / nextValue,
    '*': (prevValue, nextValue) => prevValue * nextValue,
    '+': (prevValue, nextValue) => prevValue + nextValue,
    '-': (prevValue, nextValue) => prevValue - nextValue,
    '=': (prevValue, nextValue) => nextValue
}

const initialState = {
    value: null,
    displayValue: '0',
    operator: null,
    waitingForOperand: false
}

const Calculator = () => {
    const [state, setState] = React.useState(initialState)

    function clearAll() {
        setState(initialState)
    }

    function clearDisplay() {
        setState(_state => ({
            ...state,
            displayValue: '0'
        }))
    }

    function clearLastChar() {
        const {displayValue} = state

        setState(_state => ({
            ...state,
            displayValue: displayValue.substring(0, displayValue.length - 1) || '0'
        }))
    }

    function toggleSign() {
        const {displayValue} = state
        const newValue = parseFloat(displayValue) * -1

        setState(_state => ({
            ...state,
            displayValue: String(newValue)
        }))
    }

    function inputPercent() {
        const {displayValue} = state
        const currentValue = parseFloat(displayValue)

        if (currentValue === 0)
            return

        const fixedDigits = displayValue.replace(/^-?\d*\.?/, '')
        const newValue = parseFloat(displayValue) / 100

        setState(_state => ({
            ...state,
            displayValue: String(newValue.toFixed(fixedDigits.length + 2))
        }))
    }

    function inputDot() {
        const {displayValue} = state

        if (!(/\./).test(displayValue)) {
            setState(_state => ({
                ...state,
                displayValue: displayValue + '.',
                waitingForOperand: false
            }))
        }
    }

    function inputDigit(digit) {
        const {displayValue, waitingForOperand} = state

        if (waitingForOperand) {
            setState(_state => ({
                ..._state,
                displayValue: String(digit),
                waitingForOperand: false
            }))
        } else {
            setState(_state => ({
                ..._state,
                displayValue: displayValue === '0' ? String(digit) : displayValue + digit
            }))
        }
    }

    function performOperation(nextOperator) {
        const {value, displayValue, operator} = state
        const inputValue = parseFloat(displayValue)

        if (value == null) {
            setState(_state => ({
                ..._state,
                value: inputValue
            }))
        } else if (operator) {
            const currentValue = value || 0
            const newValue = CalculatorOperations[operator](currentValue, inputValue)

            setState(_state => ({
                ..._state,
                value: newValue,
                displayValue: String(newValue)
            }))
        }

        setState(_state => ({
            ..._state,
            waitingForOperand: true,
            operator: nextOperator
        }))
    }

    const handleKeyDown = (event) => {
        let {key} = event

        if (key === 'Enter')
            key = '='

        if ((/\d/).test(key)) {
            event.preventDefault()
            inputDigit(parseInt(key, 10))
        } else if (key in CalculatorOperations) {
            event.preventDefault()
            performOperation(key)
        } else if (key === '.') {
            event.preventDefault()
            inputDot()
        } else if (key === '%') {
            event.preventDefault()
            inputPercent()
        } else if (key === 'Backspace') {
            event.preventDefault()
            clearLastChar()
        } else if (key === 'Clear') {
            event.preventDefault()

            if (state.displayValue !== '0') {
                clearDisplay()
            } else {
                clearAll()
            }
        }
    };

    React.useLayoutEffect(() => {
        document.addEventListener('keydown', handleKeyDown)

        return () => {
            document.removeEventListener('keydown', handleKeyDown)
        }
    })

    return (
        <div className="calculator">
            <CalculatorDisplay value={state.displayValue}/>
            <div className="calculator-keypad">
                <div className="input-keys">
                    <div className="function-keys">
                        <CalculatorKey className="key-clear"
                                       onPress={() => state.displayValue !== '0' ? clearDisplay() : clearAll()}>{clearDisplay ? 'C' : 'AC'}</CalculatorKey>
                        <CalculatorKey className="key-sign" onPress={() => toggleSign()}>±</CalculatorKey>
                        <CalculatorKey className="key-percent" onPress={() => inputPercent()}>%</CalculatorKey>
                    </div>
                    <div className="digit-keys">
                        <CalculatorKey className="key-0" onPress={() => inputDigit(0)}>0</CalculatorKey>
                        <CalculatorKey className="key-dot" onPress={() => inputDot()}>●</CalculatorKey>
                        <CalculatorKey className="key-1" onPress={() => inputDigit(1)}>1</CalculatorKey>
                        <CalculatorKey className="key-2" onPress={() => inputDigit(2)}>2</CalculatorKey>
                        <CalculatorKey className="key-3" onPress={() => inputDigit(3)}>3</CalculatorKey>
                        <CalculatorKey className="key-4" onPress={() => inputDigit(4)}>4</CalculatorKey>
                        <CalculatorKey className="key-5" onPress={() => inputDigit(5)}>5</CalculatorKey>
                        <CalculatorKey className="key-6" onPress={() => inputDigit(6)}>6</CalculatorKey>
                        <CalculatorKey className="key-7" onPress={() => inputDigit(7)}>7</CalculatorKey>
                        <CalculatorKey className="key-8" onPress={() => inputDigit(8)}>8</CalculatorKey>
                        <CalculatorKey className="key-9" onPress={() => inputDigit(9)}>9</CalculatorKey>
                    </div>
                </div>
                <div className="operator-keys">
                    <CalculatorKey className="key-divide" onPress={() => performOperation('/')}>÷</CalculatorKey>
                    <CalculatorKey className="key-multiply" onPress={() => performOperation('*')}>×</CalculatorKey>
                    <CalculatorKey className="key-subtract" onPress={() => performOperation('-')}>−</CalculatorKey>
                    <CalculatorKey className="key-add" onPress={() => performOperation('+')}>+</CalculatorKey>
                    <CalculatorKey className="key-equals" onPress={() => performOperation('=')}>=</CalculatorKey>
                </div>
            </div>
        </div>
    )
}

export default Calculator;
